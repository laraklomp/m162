![TBZ Logo](../x_gitressourcen/tbz_logo.png)

---

# Merkmalstypen und Skalentypen

[TOC]

## Übersicht

Das folgende Bild gibt eine gute Übersicht über die Kategorisierung von Daten.

![Skalenniveaus](./images/Skalenniveaus.jpg)



## Diskrete und Stetige Daten

Daten sind dann **diskret**, wenn es die Anzahl Ausprägungen - also möglichen Werte - abzählbar sind. 

**Beispiel**: Sie werden in einem Fragebogen an der TBZ (Abteilung IT) gefragt, welches ihr Lieblingsmodul ist. Es gibt nur eine spezifische Anzahl im Berufsbild Informatik. Die möglichen Werte sind abzählbar.

Daten sind dann **stetig**, wenn die Anzahl Ausprägungen - also möglichen Werte - **nicht** abzählbar sind.

**Beispiel:** Sie werden in einem Fragebogen an der TBZ (Abteilung IT) gefragt, ob sie Kommentare zu dem Modul m162 haben. Die möglichen Antworten sind nicht abzählbar, da jede Person beliebigen Text schreiben kann.

## Skalentypen

Unterschieden werden die folgenden drei Skalentypen:

#### Nominalskala

Die möglichen Werte / Ausprägungen stehen **nicht** in einer **Rangordnung** zueinander. Keine der Auswahlmöglichkeit ist besser als die anderen. 

**Beispiel**: Sie werden in einem Fragebogen an der TBZ (Abteilung IT) gefragt, welches ihr Lieblingsmodul ist. Die Module sind gleichwertig untereinander. 

#### Ordinalskala

Die möglichen Werte / Ausprägungen stehen in einer **Rangordnung** zueinander.  

**Beispiel**: Sie werden in einem Fragebogen an der TBZ (Abteilung IT) gefragt, welches ihre Note für das Modul m162 war. Die Noten stehen in einer Rangreihenfolge. Die Note 5 ist besser als die Note 4. 

#### Kardinalskala

Die möglichen Werte / Ausprägungen stehen in einer **Rangordnung** zueinander **und** die Abstände zwischen den einzelnen Werten sind **interpretierbar** (also auch gleichmässig).  

**Beispiel**: Sie werden in einem Fragebogen an der TBZ (Abteilung IT) gefragt, wie viel Wasser sie täglich trinken. Die Menge Wasser (z.B. in Liter, Deziliter, etc) hat gleichmässig definierte Abstände und ist in einer Rangreihenfolge. Also 2 Liter ist mehr als 1 Liter und der Unterschied zwischen 1 und 2 Liter ist gleichgross wie zwischen 2 und 3 Liter.

**Achtung**: Wieso war die Note keine Kardinalskala? Weil der Aufwand für den Schüler zwischen der Note 4 und 5 und der Note 5 und 6 nicht unbedingt gleich gross ist.

## Häufbarkeit

Mögliche Werte sind dann häufbar, wenn mehrere Ausprägungen möglich sind. 

**Beispiel nicht häufbar**: Sie werden in einem Fragebogen an der TBZ (Abteilung IT) gefragt, welches ihr **Lieblingsmodul** ist. Durch die Fragestellung wird klar, dass sie nur ein Modul auswählen können. Typische andere Ausprägungen sind z.B. das Geschlecht oder Alter einer Person.

**Beispiel häufbar**: Sie werden in einem Fragebogen an der TBZ (Abteilung IT) gefragt, welche Module sie im letzten Semester besuchten. Durch die Fragestellung wird klar, dass sie mehrere Module auswählen können. 

## Quellen / Weitere Informationen

- <https://wissenschafts-thurm.de/grundlagen-der-statistik-worin-unterscheiden-sich-diskrete-und-stetige-merkmale-und-wann-sind-merkmale-haeufbar/>

- <https://123mathe.de/merkmalsarten-und-merkmalsskalen>

- [Video Skalentypen](https://www.youtube.com/watch?v=RxN0vzAX6sU)

- <https://www.crashkurs-statistik.de/merkmals-und-skalentypen/>



---

&copy;TBZ, 2021, Modul: m162