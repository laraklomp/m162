![TBZ Logo](../x_gitressourcen/tbz_logo.png)

---

# JSON <!-- omit in toc -->

#### Tutorials

https://www.youtube.com/watch?v=iiADhChRriM&ab_channel=WebDevSimplified (12 Minuten)

https://www.w3schools.com/js/js_json_intro.asp

https://www.tutorialspoint.com/json/index.htm

https://www.json.org/json-en.html (sehr formal)

#### Tools

https://jsonformatter.curiousconcept.com/
Formatiert und validiert JSON

https://www.freeformatter.com/xml-validator-xsd.html
Validiert XML

---

&copy;TBZ, 2021, Modul: m162