![TBZ Logo](../x_gitressourcen/tbz_logo.png)

---

# Zeichensätze

[TOC]

## ASCII (American Standard Code for Information Interchange)

[ASCII](https://www.asciitable.com/) bezeichnet ein Set von Zeichen und weist diesen feste Zahlen zu. 

![asciibasic](./images/asciibasic.gif)

Der Hintergrund ist die Art und Weise wie Daten digital gespeichert werden. Jedes Zeichen (Zahlen, Buchstaben, Zeilenumbrüche, etc) werden auf der Festplatte oder im Speicher binär gespeichert. 

**Fragen**:

- Wie speichert man das Zeichen '*A*' binär?
- Wie speichert man die Zahl '*65*' binär?

Wenn sie in der Tabelle oben nachschauen, finden sie in der Spalte 3, Zeile 2 die Antwort zu der Frage. Die Zahl *65* und das Zeichen *A* sind gleichwertig, resp. das Zeichen *A* kann durch die Zahl *65* repräsentiert werden, aber auch umgekehrt! Sowohl *A* als auch *65* werden mit der binären Repräsentation *01000001* gespeichert.

Durch Metadaten weiss ein Programm, ob es sich um ein Zeichen oder eine Zahl handelt, aber eigentlich ist das egal, denn es ist das gleiche. In einem Java-Programm (und auch allen anderen Programmen) können sie einen Datentyp in den anderen umwandeln:

- `char c = 'A'; int i = c; // Die Variable i hat den Wert 65`
- `int i = 65; char c = i; // Die Variable c hat den Wert 'A'`
- `char c = '1'; int i = c; // Die Variable i hat den Wert 49 und nicht 1. Schauen sie in der ASCII-Tabelle nach.`

## 8-Bit Zeichensätze

Weil die ASCII-Tabelle nur mit 7-bit Speicherplatz arbeitet (also 7 Stellen mit 0 oder 1), reichen die Möglichkeiten bei weitem nicht, um alle Zeichen abzubilden. Als erster Schritt wurden die weiteren 128 Werte, die durch das achte Bit (somit 1 Byte pro Zeichen) möglich sind, definiert:

**ANSI** ist eine Erweiterung des ASCII-Codes, er ist genormt vom American National-Standards Institute (daher der Name) und hat sich als Standard auf den Windows- und Macintosh-Betriebssystemen in Amerika durchgesetzt.

Für andere Länder wurden diese 128 Werte mit entsprechend anderen Zeichen definiert. Um verschiedene Zeichensätze managen zu können, musste der Computer zwischen diesen sog. [Codepages](https://de.wikipedia.org/wiki/Zeichensatztabelle) umschalten.  

**ISO/IEC 8859-1 / Latin-1**: Eine Zeichensatztabelle (Codepage) die oft in Europa Verwendung findet, weil sie all die lateinischen Zeichen, sowie Umlaute abbildet. 

## Unicode

Entstanden Ende der 80er Jahre mit dem Ziel alle Sprachen der Welt in einem Zeichensatz zu vereinen, ist der [Unicode](https://de.wikipedia.org/wiki/Unicode) der grösste und umfassendste [Zeichensatz](https://unicode-table.com/de/). Anfangs 16-Bit codiert, allerdings 2001 umgestellt auf **32-Bit** beinhaltete Unicode 4 im Jahre 2003 ca. 100000 verschiedene Zeichen. Der Unicode vereint tote wie auch lebende Sprachen, so sind z.B. auch Runen Bestandteil.
Jedes Zeichen wird also mit **4 Byte** abgespeichert (=**UTF-32**). Um diesen Speicherplatzhunger zu bändigen gibt es zwei Codierungen, welche die Anzahl der Speicherzellen bei den am häufigsten verwendeten Zeichen auf 2 Byte (**UTF-16**, siehe **JAVA**), bzw. sogar bis auf 1 Byte (**UTF-8**) reduzieren.


### UTF-8
[UTF-8](https://en.wikipedia.org/wiki/UTF-8) ist die häufigste verwendete Kodierung für Unicode-Zeichen. Der Speicherplatz für ein beliebiges Zeichen kann von einem bis zu vier Bytes betragen! (Siehe Beispiele unten.) Die ersten 128 Zeichen entsprechen der ASCII-Tabelle (1 Byte). 

Die Abkürzung bedeutet „UCS Transformation Format 8-Bit“. Die Abkürzung UCS wiederum steht für Universal Character Set. Dieser Zeichensatz ist besonders im Internet weit verbreitet.  
Dies ist allerdings keine Verpflichtung, sondern nur eine Empfehlung oder "Bitte". 
Und das macht das Informatikerleben nicht einfacher. Aber immerhin: im Mai 2017 verwendeten 89,0 % aller Websites UTF-8, im März 2018 bereits 91,4 % und im März 2020 95%

**Beispiel Zeichen 'ß': (UTF-8 = 2 Byte)**

![ß](./images/Scharfes_S.png)

**Beispiel Zeichen '➞':(UTF-8 = 3 Byte)**

![-->](./images/PfeilRechts.png)

**Beispiel Zeichen '😄': (UTF-8 = 4 Byte)**

![😄](./images/Smiley.png)

---

&copy;TBZ, 2022, Modul: m162