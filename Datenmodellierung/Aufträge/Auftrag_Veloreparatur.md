![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Veloreparatur": Normalisierung

In dieser Übung üben sie die Normalisierungschritte und arbeiten mit MySql Workbench.

Sie betreiben einen Veloreparaturdienst als Start-Up. Bisher hatten sie ihre Kunden in einem Excel gepflegt. Nun wird ihre Kundenbasis aber immer grösser und sie sehen die Probleme die sie bekommen mit dem editieren des Excels (Anomalien). Sie möchten umstellen auf eine richtige Datenbank...

#### Aufgabe

Normalisieren sie die folgende Tabelle und gehen sie dabei Schritteweise vor. Die Tabelle finden sie [hier als Excel](Auftrag_Veloreparatur.xlsx).

![Bild](x_gitressourcen/Veloreparatur.png)

**Tasks**:

- Erstellen sie die erste Normalform in Excel.
- Erstellen sie die zweite Normalform in draw.io.
  - Erstellen sie ein konzeptionelles ERD mit den erkannten Entitäten aus der 1. Normalform
  - Erstellen sie ein logisches ERD und folgen sie den Schritten
  - Überprüfen sie in Excel, ob ihr logisches Schema standhält und erstellen sie die neue Struktur mit Daten.
- Erstellen sie die dritte Normalform aufgrund der 2. Normalform
  - Arbeiten sie direkt in MySql Workbench

#### Zeit und Form

- 30 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162