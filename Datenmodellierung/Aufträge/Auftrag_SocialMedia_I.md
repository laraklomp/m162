![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag: ERD zu "Social Media" definieren

In dieser Übung geht es darum, dass sie aufgrund einer Beschreibung mit Kriterien ein ERD erstellen können. 

#### Aufgabe

Erstellen sie ein ERD zum Thema "Social Media", welches die folgenden Kriterien erfüllt.

Wir möchten eine neue Social Media Applikation erstellen, die spezifisch nur von allen Lehrpersonen und Lernenden der Abteilung IT von der TBZ verwendet können. Es gibt keine öffentlichen Einträge. Jeder Nutzer (Lehrperson oder Lernende) kann einen Text-Post erstellen. Diesen Post kann man liken, aber nicht kommentieren oder darauf antworten. Posts von Lernenden können nur von den Lernenden der gleichen Klasse gelesen werden, aber auch von allen Lehrpersonen. Eine Lehrperson kann entweder einen Post für eine seiner Klassen erstellen oder einen Post für andere Lehrpersonen.

**Tasks**:

- Erstellen sie das ERD mit einem Tool (z.B. draw.io)
- Dokumentieren sie ihr ERD und erklären sie wie sie die Kriterien erfüllen können und was ihre Beziehungen bedeuten.
  
#### Zeit und Form

- 20 Minuten
- 2er Gruppen

---

&copy;TBZ, 2021, Modul: m162