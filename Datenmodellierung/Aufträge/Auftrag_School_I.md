![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "School I": Redundanzen und 1. Normalform

Sie erkennen Redundanzen in Daten und leiten die 1. Normalform ab

#### Aufgabe

Schauen sie sich die folgenden Daten/Tabelle an. Erkennen sie Redundanzen und bringen sie diese in die erste Normalform. Verwenden sie [diese Excel-Vorlage](Auftrag_School_I.xlsx) als Basis für die Umwandlung in die erste Normalform.

![daten](x_gitressourcen/School_I.png)

#### Zeit und Form

- 20 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162
